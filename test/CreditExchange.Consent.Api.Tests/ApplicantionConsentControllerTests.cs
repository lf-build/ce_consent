﻿using CreditExchange.Application.Document;
using CreditExchange.ApplicationConsent.Mocks;
using LendFoundry.Consent.Api.Controllers;
using LendFoundry.Consent.Mocks;
using LendFoundry.DocumentGenerator;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LendFoundry.TemplateManager;
#if DOTNET2
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
#endif
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Consent.Api.Tests
{
    public class ApplicantionConsentControllerTests
    {
        #region PrivateMethods
        private static ConsentController GetController(List<IConsent> ApplicationConsent )
        {
            return new ConsentController(new FakeApplicationConsentService(new UtcTenantTime(), ApplicationConsent));
        }

        private static ConsentController GetControllerWithOutFakeService(List<IConsent> ApplicationConsent)
        {
            return new ConsentController(GetConsentMockObject(ApplicationConsent));
        }
        private static IConsentService GetConsentMockObject(IEnumerable<IConsent> consentsdata = null)
        {
            return new ConsentService(new FakeApplicationConsentRepository(new UtcTenantTime(), consentsdata ?? new List<IConsent>()), Mock.Of<IEventHubClient>(), new UtcTenantTime(), Mock.Of<IDocumentGeneratorService>(), Mock.Of<ITemplateManagerService>(), Mock.Of<CreditExchange.Consent.ConsentConfiguration>(), Mock.Of<IApplicationDocumentService>(), Mock.Of<IHttpContextAccessor>());
        }
      
        public ApplicantionConsentControllerTests()
        {
        }

        
        private static List<CreditExchange.Consent.IConsent> GetDummyDataOfConsent()
        {
            List<IConsent> consentList = new List<IConsent>();

            IConsent consentData = new Consent
            {
                ConsentName = "Test",
                DocumentId = "1",
                EntityId = "1",
                EntityType = "testEntity",
                Id = "1",
                IpAddress = "TestIp"
            };
            consentList.Add(consentData);
            return consentList;
        }

        #endregion

        #region GetSignedConsentDocument

        [Fact]
        public void GetSignedConsentDocument_Should_Return_Ok()
        {
            var response = GetController(GetDummyDataOfConsent()).GetSignedConsentDocument("testEntity", "1");
            Assert.IsType<HttpOkObjectResult>(response.Result);         

        }


        #endregion

        #region View
        [Fact]
        public void View_Should_Return_Ok()
        {
            var response = GetController(GetDummyDataOfConsent()).View("testEntity", "1", "Test","Test");
            Assert.IsType<HttpOkObjectResult>(response.Result);

        }

        #endregion


        #region Sign
        [Fact]
        public void Sign_Should_Return_Ok()
        {
            var response = GetController(GetDummyDataOfConsent()).Sign("testEntity", "1", "Test", "Test");
            Assert.IsType<HttpOkObjectResult>(response.Result);

        }

        [Fact]
        public void Sign_Should_Return_400()
        {
            var response = GetControllerWithOutFakeService(GetDummyDataOfConsent()).Sign(null, "1", "Test", "Test");
            Assert.IsType<ErrorResult>(response.Result);
            var result = ((IActionResult)response.Result) as ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

        }

        [Fact]
        public void Sign_Should_Return_404()
        {
            var response = GetControllerWithOutFakeService(GetDummyDataOfConsent()).Sign("testEntity1", "1", "Test", "Test");
            Assert.IsType<ErrorResult>(response.Result);
            var result = ((IActionResult)response.Result) as ErrorResult;
            Assert.ThrowsAsync<ArgumentNullException>(() => { throw new ArgumentNullException("cannot be empty"); }); // temp

            //Assert.NotNull(result);
            //Assert.Equal(result.StatusCode, 404);

        }




        #endregion
    }
}
