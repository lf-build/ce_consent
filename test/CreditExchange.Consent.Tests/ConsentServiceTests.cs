﻿using CreditExchange.Application.Document;
using CreditExchange.ApplicationConsent.Mocks;
using LendFoundry.Consent;
using LendFoundry.Consent.Configuration;
using LendFoundry.DocumentGenerator;
using LendFoundry.DocumentManager;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LendFoundry.TemplateManager;
using Microsoft.AspNet.Http;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CreditExchange.ApplicationConsent.Tests
{
    public class ConsentServiceTests
    {
        #region PrivateMethods
        private static IConsentService GetConsentMockObject(IEnumerable<IConsent> consentsdata = null)
        {
            return new ConsentService(new FakeApplicationConsentRepository(new UtcTenantTime(), consentsdata ?? new List<IConsent>()), Mock.Of<IEventHubClient>(), new UtcTenantTime(), Mock.Of<IDocumentGeneratorService>(), Mock.Of<ITemplateManagerService>(), Mock.Of<CreditExchange.Consent.ConsentConfiguration>(),Mock.Of<IApplicationDocumentService>(), Mock.Of<IHttpContextAccessor>());
        }

        private static IConsentService GetConsentWithConsentConfigurationNull(IEnumerable<IConsent> consentsdata = null)
        {
            return new ConsentService(new FakeApplicationConsentRepository(new UtcTenantTime(), consentsdata ?? new List<IConsent>()), Mock.Of<IEventHubClient>(), new UtcTenantTime(), Mock.Of<IDocumentGeneratorService>(), Mock.Of<ITemplateManagerService>(), null,Mock.Of<IApplicationDocumentService>(), Mock.Of<IHttpContextAccessor>());
        }

        private static IConsentService GetConsentWithConsentConfigurationObject(ITemplateManagerService templateManagerService = null, IEnumerable<IConsent> consentsdata = null, CreditExchange.Consent.ConsentConfiguration consentConfigData = null)
        {
            return new ConsentService(new FakeApplicationConsentRepository(new UtcTenantTime(), consentsdata ?? new List<IConsent>()),Mock.Of<IEventHubClient>(), new UtcTenantTime(), Mock.Of<IDocumentGeneratorService>(), templateManagerService, consentConfigData, Mock.Of<IApplicationDocumentService>(), Mock.Of<IHttpContextAccessor>());
        }


        private static IConsentService GetConsentWithDocumentConfiguration(IDocumentGeneratorService documentGenerateService = null,IDocumentManagerService documentManagerService = null, IEnumerable<IConsent> consentsdata = null, CreditExchange.Consent.ConsentConfiguration consentConfigData = null)
        {
            return new ConsentService(new FakeApplicationConsentRepository(new UtcTenantTime(), consentsdata ?? new List<IConsent>()), Mock.Of<IEventHubClient>(), new UtcTenantTime(), documentGenerateService, Mock.Of<ITemplateManagerService>(), consentConfigData, Mock.Of<IApplicationDocumentService>(), Mock.Of<IHttpContextAccessor>());
        }

        private static List<CreditExchange.Consent.Consent> GetDummyDataOfConsent()
        {
            return new List<CreditExchange.Consent.Consent>
            {
                new CreditExchange.Consent.Consent
                {
                    ConsentName = "Test",
                     DocumentId ="1",
                      EntityId = "1",
                       EntityType= "testEntity",
                        Id = "1",
                         IpAddress= "TestIp"


                },
                 new CreditExchange.Consent.Consent
                {
                     ConsentName = "testConsent",
                     DocumentId ="2",
                      EntityId = "1",
                       EntityType= "testEntity",
                        Id = "2",
                         IpAddress= "TestIp"
                }
            };
        }

        private static List<CreditExchange.Consent.Consent> GetDummyDataOfConsentUnique()
        {
            return new List<CreditExchange.Consent.Consent>
            {
                new CreditExchange.Consent.Consent
                {
                    ConsentName = "Test",
                     DocumentId ="1",
                      EntityId = "1",
                       EntityType= "testEntity",
                        Id = "1",
                         IpAddress= "TestIp"


                },
                 new CreditExchange.Consent.Consent
                {
                     ConsentName = "test2",
                     DocumentId ="2",
                      EntityId = "2",
                       EntityType= "testEntity",
                        Id = "2",
                         IpAddress= "TestIp"
                }
            };
        }

        private static ConsentConfigurationEntity[] GetConfigurationEntityData()
        {
            ConsentConfigurationEntity[] consentConfigurationEntity = new ConsentConfigurationEntity[1];
            consentConfigurationEntity[0] = new ConsentConfigurationEntity();
            consentConfigurationEntity[0].EntityName = "testEntity";
            consentConfigurationEntity[0].Consents = GetConsentCofigurationData();
            return consentConfigurationEntity;
        }

        private static ConsentConfigurationEntity[] GetConfigurationEntityDataWithConsentNull()
        {
            ConsentConfigurationEntity[] consentConfigurationEntity = new ConsentConfigurationEntity[1];
            consentConfigurationEntity[0] = new ConsentConfigurationEntity();
            consentConfigurationEntity[0].EntityName = "testEntity";
            consentConfigurationEntity[0].Consents = null;
            return consentConfigurationEntity;
        }

        private static ITemplate GetTemplateDummyData()
        {
            ITemplate templateData = new Template();
            templateData.Name = "testTemplate";
            templateData.Version = "1";
            templateData.Format = Format.Html;
            templateData.Title = "templateTitle";
            return templateData;
        }

        private static ITemplateResult GetTemplateResultDummyData()
        {
            ITemplateResult templateData = new TemplateResult();
            templateData.Data = "testData";
            templateData.Title = "templateTitle";
            return templateData;
        }

        private static ConsentConfigurationConsent[] GetConsentCofigurationData()
        {
            var consentConfiguration = new ConsentConfigurationConsent[1];

            consentConfiguration[0] = new ConsentConfigurationConsent();
            consentConfiguration[0].Name = "testConsent";
            consentConfiguration[0].TemplateVersion = "1";
            consentConfiguration[0].Document=new ConsentTrackerDocument();
            consentConfiguration[0].Document.Name = "testConsent";
            consentConfiguration[0].Document.Tags = new List<string> { "1", "2" };

            return consentConfiguration;

        }

        private static Consent.ConsentConfiguration GetDummyDataOfConsentConfiguration()
        {

            return new Consent.ConsentConfiguration
            {
                Entities = GetConfigurationEntityData()

            };


        }

        private static Consent.ConsentConfiguration GetDummyDataOfConsentConfigurationWithConsentNull()
        {

            return new Consent.ConsentConfiguration
            {
                Entities = GetConfigurationEntityDataWithConsentNull()

            };


        }

        private static LendFoundry.DocumentGenerator.IDocument GetDocumentDummyData()
        {
           return  new LendFoundry.DocumentGenerator.Document
            {
                Data = "Test",
                Format = DocumentFormat.Pdf,
                Name = "testConsent",
                Version = "1.0" //TODO: check how to implement versioning
            };
        }

        private static byte[] GetBytes()
        {
            string input = "some text";
            byte[] array = Encoding.ASCII.GetBytes(input);
            return array;
        }

        private static LendFoundry.DocumentManager.IDocument GetDummyDocumentDataWithNullId()
        {
            LendFoundry.DocumentManager.IDocument documentData = new LendFoundry.DocumentManager.Document();
            documentData.Id = null;
            documentData.FileName = "Test";
            return documentData;
        }

        private static LendFoundry.DocumentManager.IDocument GetDummyDocumentDataWithValidOutPut()
        {
            LendFoundry.DocumentManager.IDocument documentData = new LendFoundry.DocumentManager.Document();
            documentData.Id = "1";
            documentData.FileName = "Test";
            return documentData;
        }
        private static object GetObjectDummyData()
        {
            string s = "{\"IPAddress\":\"10.1.1.99\" , \"SignedBy\": \"1\"}";
            dynamic jsonResponse = JsonConvert.DeserializeObject(s);

            return jsonResponse;
          
        }

        #endregion

        #region GetSignedConsentDocument

        [Fact]
        public void GetSignedConsentDocument_Should_Throw_Exception_When_entityType_Is_Null()
        {
            var consentService = GetConsentMockObject();
            consentService.GetSignedConsentDocument(null, "entityId");

            Assert.ThrowsAsync<ArgumentException>(() => { throw new ArgumentException("cannot be empty"); });

        }

        [Fact]
        public void GetSignedConsentDocument_Should_Throw_Exception_When_entityId_Is_Null()
        {
            var consentService = GetConsentMockObject();
            consentService.GetSignedConsentDocument("entityType", null);

            Assert.ThrowsAsync<ArgumentException>(() => { throw new ArgumentException("cannot be empty"); });

        }

        [Fact]
        public void GetSignedConsentDocument_Should_Throw_Exception_When_Document_Is_Not_Found()
        {
            var consentService = GetConsentMockObject(GetDummyDataOfConsent());
            consentService.GetSignedConsentDocument("entityType", "entityId");

            Assert.ThrowsAsync<NotFoundException>(() => { throw new NotFoundException("No signed consent is found for"); });

        }

        [Fact]
        public void GetSignedConsentDocument_Should_Get_ConsentObject_When_Input_Are_Valid()
        {
            var consentService = GetConsentMockObject(GetDummyDataOfConsent());
            var consentData = consentService.GetSignedConsentDocument("testEntity", "1");

            Assert.NotNull(consentData);

        }

        #endregion

        #region View

        [Fact]
        public void View_Should_Throw_Exception_When_EntityType_Is_Null()
        {
            var consentService = GetConsentWithConsentConfigurationNull();
            consentService.View(null, "entityId", "consentName", "Test");

            Assert.ThrowsAsync<ArgumentNullException>(() => { throw new ArgumentNullException("cannot be empty"); });

        }

        [Fact]
        public void View_Should_Throw_Exception_When_ConsentName_Is_Null()
        {
            var consentService = GetConsentWithConsentConfigurationNull();
            consentService.View("etityType", "entityId",null, "Test");

            Assert.ThrowsAsync<ArgumentNullException>(() => { throw new ArgumentNullException("cannot be empty"); });

        }

        [Fact]
        public void View_Should_Throw_Exception_When_ConsentConfiguration_Is_Null()
        {
            var consentService = GetConsentWithConsentConfigurationNull();
            consentService.View("entityType", "entityId", "consentName", "Test");

            Assert.ThrowsAsync<NotFoundException>(() => { throw new NotFoundException("cannot be empty"); });

        }

        [Fact]
        public void View_Should_Throw_Exception_When_EntityType_Not_Found()
        {
            var consentService = GetConsentWithConsentConfigurationObject(null,GetDummyDataOfConsent(), GetDummyDataOfConsentConfiguration());
            consentService.View("testEntity1", "entityId", "testConsent", "Test");

            Assert.ThrowsAsync<NotFoundException>(() => { throw new NotFoundException("cannot be empty"); });

        }

        [Fact]
        public void View_Should_Throw_Exception_When_Consent_Is_Null()
        {
            var consentService = GetConsentWithConsentConfigurationObject(null,GetDummyDataOfConsent(), GetDummyDataOfConsentConfigurationWithConsentNull());
            consentService.View("testEntity", "entityId", "testConsent", "Test");

            Assert.ThrowsAsync<NotFoundException>(() => { throw new NotFoundException("cannot be empty"); });

        }

        [Fact]
        public void View_Should_Throw_Exception_When_configConsent_Value_Is_Null()
        {
            var consentService = GetConsentWithConsentConfigurationObject(null,GetDummyDataOfConsent(), GetDummyDataOfConsentConfigurationWithConsentNull());
            consentService.View("testEntity", "entityId", "testConsent1", "Test");

            Assert.ThrowsAsync<NotFoundException>(() => { throw new NotFoundException("cannot be empty"); });

        }


        [Fact]
        public void View_Should_Throw_Exception_When_Template_Is_Not_Found()
        {
            Mock<ITemplateManagerService> _mockTemplateManagerService = new Mock<ITemplateManagerService>();
            _mockTemplateManagerService.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>())).ReturnsAsync(null);
            var consentService = GetConsentWithConsentConfigurationObject(_mockTemplateManagerService.Object,GetDummyDataOfConsent(), GetDummyDataOfConsentConfiguration());
            consentService.View("testEntity", "entityId", "testConsent1", "Test");

            Assert.ThrowsAsync<NotFoundException>(() => { throw new NotFoundException("cannot be empty"); });

        }

        [Fact]
        public void View_Should_Throw_Exception_When_TemplateResult_Not_Found()
        {
            Mock<ITemplateManagerService> _mockTemplateManagerService = new Mock<ITemplateManagerService>();
            _mockTemplateManagerService.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>())).ReturnsAsync(GetTemplateDummyData());

            _mockTemplateManagerService.Setup(x => x.Process(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>(),It.IsAny<object>())).ReturnsAsync(null);
            var consentService = GetConsentWithConsentConfigurationObject(_mockTemplateManagerService.Object, GetDummyDataOfConsent(), GetDummyDataOfConsentConfiguration());
            consentService.View("testEntity", "entityId", "testConsent1", "Test");

            Assert.ThrowsAsync<NotFoundException>(() => { throw new NotFoundException("cannot be empty"); });

        }

        [Fact]
        public void View_Should_Give_Template_When_Inputs_Are_Valid()
        {
            Mock<ITemplateManagerService> _mockTemplateManagerService = new Mock<ITemplateManagerService>();
            _mockTemplateManagerService.Setup(x => x.Get(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>())).ReturnsAsync(GetTemplateDummyData());

            _mockTemplateManagerService.Setup(x => x.Process(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>(), It.IsAny<object>())).ReturnsAsync(GetTemplateResultDummyData());
            var consentService = GetConsentWithConsentConfigurationObject(_mockTemplateManagerService.Object, GetDummyDataOfConsent(), GetDummyDataOfConsentConfiguration());
           var templateResult = consentService.View("testEntity", "entityId", "testConsent", "Test");

            Assert.Equal(templateResult.Result.Title, "templateTitle");

        }


        #endregion

        #region SignDocument
        [Fact]
        public void Sign_Should_Throw_Exception_When_entityType_Is_Null()
        {
            var consentService = GetConsentMockObject();
            consentService.Sign(null, "entityId", "consentName", GetObjectDummyData());

            Assert.ThrowsAsync<ArgumentNullException>(() => { throw new ArgumentNullException("cannot be empty"); });

        }

        [Fact]
        public void Sign_Should_Throw_Exception_When_entityId_Is_Null()
        {
            var consentService = GetConsentMockObject();
            consentService.Sign("entityType", null, "consentName", GetObjectDummyData());

            Assert.ThrowsAsync<ArgumentNullException>(() => { throw new ArgumentNullException("cannot be empty"); });

        }

        [Fact]
        public void Sign_Should_Throw_Exception_When_consentName_Is_Null()
        {
            var consentService = GetConsentMockObject();
            consentService.Sign("entityType", "entityId", null, GetObjectDummyData());

            Assert.ThrowsAsync<ArgumentNullException>(() => { throw new ArgumentNullException("cannot be empty"); });

        }

        [Fact]
        public void Sign_Should_Throw_Exception_When_ConsentConfiguration_Is_Null()
        {
            var consentService = GetConsentWithConsentConfigurationNull();
            consentService.Sign("entityType", "entityId", "consentName", "Test");

            Assert.ThrowsAsync<NotFoundException>(() => { throw new NotFoundException("cannot be empty"); });
        

        }

        [Fact]
        public void Sign_Should_Throw_Exception_When_EntityType_Not_Found()
        {
            var consentService = GetConsentWithConsentConfigurationObject(null, GetDummyDataOfConsent(), GetDummyDataOfConsentConfiguration());
            consentService.Sign("testEntity1", "entityId", "testConsent", "Test");

            Assert.ThrowsAsync<NotFoundException>(() => { throw new NotFoundException("cannot be empty"); });


        }

        [Fact]
        public void Sign_Should_Throw_Exception_When_Consent_Is_Null()
        {
            var consentService = GetConsentWithConsentConfigurationObject(null, GetDummyDataOfConsent(), GetDummyDataOfConsentConfigurationWithConsentNull());
            consentService.Sign("testEntity", "entityId", "testConsent", "Test");

            Assert.ThrowsAsync<NotFoundException>(() => { throw new NotFoundException("cannot be empty"); });


        }

        [Fact]
        public void Sign_Should_Throw_Exception_When_configConsent_Value_Is_Null()
        {
            var consentService = GetConsentWithConsentConfigurationObject(null, GetDummyDataOfConsent(), GetDummyDataOfConsentConfigurationWithConsentNull());
            consentService.Sign("testEntity", "entityId", "testConsent1", GetObjectDummyData());

            Assert.ThrowsAsync<NotFoundException>(() => { throw new NotFoundException("cannot be empty"); });


        }

        [Fact]
        public void Sign_Should_Throw_Already_Exists_Exception_When_ConsentFind()
        {
            var consentService = GetConsentWithConsentConfigurationObject(null, GetDummyDataOfConsent(), GetDummyDataOfConsentConfiguration());
            consentService.Sign("testEntity", "1", "testConsent", GetObjectDummyData());

            Assert.ThrowsAsync<ConsentAlreadySigned>(() => { throw new ConsentAlreadySigned("cannot be empty"); });


        }

        [Fact]
        public void Sign_Should_Throw_Exception_When_DocumentResult_Is_Null()
        {
            Mock<IDocumentManagerService> _mockDocumentManagerService = new Mock<IDocumentManagerService>();
            Mock<IDocumentGeneratorService> _mockDocumentGeneratorService = new Mock<IDocumentGeneratorService>();
            _mockDocumentGeneratorService.Setup(x => x.Generate(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>(), It.IsAny<LendFoundry.DocumentGenerator.IDocument>())).ReturnsAsync(null);


                var consentService = GetConsentWithDocumentConfiguration(_mockDocumentGeneratorService.Object, _mockDocumentManagerService.Object, GetDummyDataOfConsent(), GetDummyDataOfConsentConfiguration());
            consentService.Sign("testEntity", "entityId", "testConsent", GetObjectDummyData());

            Assert.ThrowsAsync<NotFoundException>(() => { throw new NotFoundException("cannot be empty"); });


        }


        [Fact]
        public void Sign_Should_Throw_Exception_When_Uploaded_Document_Is_Null()
        {
            //Mock<IDocumentManagerService> _mockDocumentManagerService = new Mock<IDocumentManagerService>();
            //Mock<IDocumentGeneratorService> _mockDocumentGeneratorService = new Mock<IDocumentGeneratorService>();
            //_mockDocumentGeneratorService.Setup(x => x.Generate(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>(), It.IsAny<LendFoundry.DocumentGenerator.IDocument>())).ReturnsAsync(GetBytes());

            //_mockDocumentManagerService.Setup(x => x.Create(It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<string>>())).ReturnsAsync(GetDummyDocumentDataWithNullId());


            //var consentService = GetConsentWithDocumentConfiguration(_mockDocumentGeneratorService.Object, _mockDocumentManagerService.Object, GetDummyDataOfConsent(), GetDummyDataOfConsentConfiguration());
            //consentService.Sign("testEntity", "entityId", "testConsent", GetObjectDummyData());

            //Assert.ThrowsAsync<ConcentException>(() => { throw new ConcentException("cannot be empty"); });


        }


        [Fact]
        public void Sign_Should_Sign_Document_When_Inputs_Are_Valid()
        {
           //  string[] test = new string[] { "1", "2" };
           // Mock<IDocumentManagerService> _mockDocumentManagerService = new Mock<IDocumentManagerService>();
           // Mock<IDocumentGeneratorService> _mockDocumentGeneratorService = new Mock<IDocumentGeneratorService>();
           // _mockDocumentGeneratorService.Setup(x => x.Generate(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Format>(), It.IsAny<LendFoundry.DocumentGenerator.IDocument>())).ReturnsAsync(GetBytes());

           // _mockDocumentManagerService.Setup(x => x.Create(It.IsAny<Stream>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),It.IsAny<List<string>>())).ReturnsAsync(GetDummyDocumentDataWithValidOutPut());


           // var consentService = GetConsentWithDocumentConfiguration(_mockDocumentGeneratorService.Object, _mockDocumentManagerService.Object, GetDummyDataOfConsent(), GetDummyDataOfConsentConfiguration());
           //var Consent =  consentService.Sign("testEntity", "entityId", "testConsent", GetObjectDummyData());

           //// Assert.ThrowsAsync<ArgumentNullException>(() => { throw new ArgumentNullException("cannot be empty"); }); // temp

           // Assert.Equal(Consent.Result.EntityId, "entityId");


        }

        #endregion  
    }
}
