﻿using LendFoundry.Consent.Client;
using LendFoundry.Foundation.Services;
using LendFoundry.TemplateManager;
using Moq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Consent.Client.Tests
{
    public class ConsentServiceClientTests
    {
        private ConsentService ConsentServiceClient { get; }
        private IRestRequest Request { get; set; }

        private Mock<IServiceClient> MockServiceClient { get; }

        public ConsentServiceClientTests()
        {
            MockServiceClient = new Mock<IServiceClient>();
            ConsentServiceClient = new ConsentService(MockServiceClient.Object);
        }
        private static List<CreditExchange.Consent.Consent> GetDummyDataOfConsent()
        {
            return new List<CreditExchange.Consent.Consent>
            {
                new CreditExchange.Consent.Consent
                {
                    ConsentName = "Test",
                     DocumentId ="1",
                      EntityId = "1",
                       EntityType= "xyz",
                        Id = "1",
                         IpAddress= "TestIp"


                },
                 new CreditExchange.Consent.Consent
                {
                     ConsentName = "Test1",
                     DocumentId ="2",
                      EntityId = "2",
                       EntityType= "xyz",
                        Id = "2",
                         IpAddress= "TestIp"
                }
            };
        }

        private static List<TemplateResult> GetDummyDataOfTemplate()
        {
            return new List<TemplateResult>
            {
                new TemplateResult
                {
                     Data ="Test",
                     Title = "1",
                },
                new TemplateResult
                {
                     Data ="Test1",
                     Title = "2",
                }
            };
        }


        [Fact]
        public async void Client_Sign()
        {
            List<CreditExchange.Consent.Consent> dummyData = GetDummyDataOfConsent();
            MockServiceClient.Setup(s => s.ExecuteAsync<CreditExchange.Consent.Consent>(It.IsAny<IRestRequest>()))
               .ReturnsAsync(dummyData.FirstOrDefault())
                .Callback<IRestRequest>(r => Request = r);



            var result = await ConsentServiceClient.Sign("xyz","1","Test","Test");
            Assert.Equal("/{entityType}/{entityId}/{consentName}/sign", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_View()
        {
            List<TemplateResult> dummyData = GetDummyDataOfTemplate();
            MockServiceClient.Setup(s => s.ExecuteAsync<TemplateResult>(It.IsAny<IRestRequest>()))
               .ReturnsAsync(dummyData.FirstOrDefault())
                .Callback<IRestRequest>(r => Request = r);


            var result = await ConsentServiceClient.View("xyz", "1", "Test", "Test");
            Assert.Equal("/{entityType}/{concentName}/view", Request.Resource);
            Assert.Equal(Method.POST, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_GetSignedConsentDocument()
        {
            List<CreditExchange.Consent.Consent> dummyData = GetDummyDataOfConsent();
            MockServiceClient.Setup(s => s.ExecuteAsync<List<CreditExchange.Consent.Consent>>(It.IsAny<IRestRequest>()))
               .ReturnsAsync(dummyData)
                .Callback<IRestRequest>(r => Request = r);



            var result = await ConsentServiceClient.GetSignedConsentDocument("xyz" ,"1");
            Assert.Equal("/{entityType}/{entityId}", Request.Resource);
            Assert.Equal(Method.GET, Request.Method);
            Assert.NotNull(result);
        }

    }
}
