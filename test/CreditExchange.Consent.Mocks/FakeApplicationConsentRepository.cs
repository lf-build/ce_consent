﻿using LendFoundry.Consent;
using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;
using System.Linq.Expressions;


namespace CreditExchange.ApplicationConsent.Mocks
{
    public class FakeApplicationConsentRepository : IConsentRepository
    {
        public List<IConsent> ApplicationConsent { get; } = new List<IConsent>();

        public ITenantTime TenantTime { get; set; }
        public FakeApplicationConsentRepository(ITenantTime tenantTime, IEnumerable<IConsent> applicationDocuments) : this(tenantTime)
        {
            ApplicationConsent.AddRange(applicationDocuments);
        }

        public FakeApplicationConsentRepository(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        public async Task<IList<IConsent>> GetSignedConsentDocument(string entityType, string entityId)
        {
            var result = Task.FromResult(ApplicationConsent.Where(consent => consent.EntityId == entityId && consent.EntityType == entityType).ToList());
            return await result;
        }

        public Task<IConsent> GetConsentDocumentByConsentName(string entityType, string entityId, string consentName)
        {
            var result = Task.FromResult(ApplicationConsent.Where(
                        consent =>
                            consent.EntityType.Equals(entityType) &&
                            consent.EntityId.Equals(entityId) && consent.ConsentName.Equals(consentName)).FirstOrDefault());
            return result;
        }

        public Task<IConsent> Get(string id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IConsent>> All(Expression<Func<IConsent, bool>> query, int? skip, int? quantity)
        {
            throw new NotImplementedException();
        }

        public void Add(IConsent item)
        {
            ApplicationConsent.Add(item);
        }

        public void Remove(IConsent item)
        {
            throw new NotImplementedException();
        }

        public void Update(IConsent item)
        {
            throw new NotImplementedException();
        }

        public int Count(Expression<Func<IConsent, bool>> query)
        {
            throw new NotImplementedException();
        }
    }
}
