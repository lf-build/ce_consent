﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.TemplateManager;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Consent.Mocks
{
    public class FakeApplicationConsentService : IConsentService
    {
        public List<IConsent> ApplicationConsent { get; } = new List<IConsent>();
        #region PrivateMethos

        private static List<CreditExchange.Consent.Consent> GetDummyDataOfConsent()
        {
            return new List<CreditExchange.Consent.Consent>
            {
                new CreditExchange.Consent.Consent
                {
                    ConsentName = "Test",
                     DocumentId ="1",
                      EntityId = "1",
                       EntityType= "testEntity",
                        Id = "1",
                         IpAddress= "TestIp"


                },
                 new CreditExchange.Consent.Consent
                {
                     ConsentName = "Test1",
                     DocumentId ="2",
                      EntityId = "2",
                       EntityType= "xyz",
                        Id = "2",
                         IpAddress= "TestIp"
                }
            };
        }

        public FakeApplicationConsentService(ITenantTime tenantTime, IEnumerable<IConsent> applicationDocuments) : this(tenantTime)
        {
            ApplicationConsent.AddRange(applicationDocuments);
        }

        public FakeApplicationConsentService(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        public ITenantTime TenantTime { get; set; }
        #endregion

        public async Task<IList<IConsent>> GetSignedConsentDocument(string entityType, string entityId)
        {
            return await Task.FromResult(ApplicationConsent);
        }

      public async  Task<IConsent> Sign(string entityType, string entityId, string concentName, object body)
        {
            IConsent consentData = new Consent
            {
                ConsentName = "Test",
                DocumentId = "1",
                EntityId = "1",
                EntityType = "testEntity",
                Id = "1",
                IpAddress = "TestIp"
            };
            return await Task.FromResult(consentData);
        }

       public async Task<ITemplateResult> View(string entityType, string entityId, string consentName, object data)
        {
            ITemplateResult templateData = new TemplateResult();
            templateData.Data = "testData";
            templateData.Title = "templateTitle";
            return await Task.FromResult(templateData);
        }
    }
}
