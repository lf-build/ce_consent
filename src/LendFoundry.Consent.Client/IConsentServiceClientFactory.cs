﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Consent.Client
{
    public interface IConsentServiceClientFactory
    {
        IConsentService Create(ITokenReader reader);
    }
}
