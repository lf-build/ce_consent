﻿using LendFoundry.Foundation.Client;
using LendFoundry.TemplateManager;
using Newtonsoft.Json;
using RestSharp;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Consent.Client
{
    public class ConsentService : IConsentService
    {
        public ConsentService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<IConsent> Sign(string entityType, string entityId, string consentName, object body)
        {
            var request = new RestRequest("/{entityType}/{entityId}/{consentName}/sign", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("consentName", consentName);            
            var json = JsonConvert.SerializeObject(body);
            request.AddParameter("text/json", json, ParameterType.RequestBody);
            return await Client.ExecuteAsync<Consent>(request);
            //return await Client.ExecuteAsync<List<Consent>>(request);
        }

        public async Task<ITemplateResult> View(string entityType, string entityId, string consentName, object data)
        {
            var request = new RestRequest("/{entityType}/{concentName}/view", Method.POST);
            request.AddUrlSegment("entityType", entityType);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("concentName", consentName);
            request.AddJsonBody(data);
            return await Client.ExecuteAsync<TemplateResult>(request);
        }

        public async Task<IList<IConsent>> GetSignedConsentDocument(string entityType, string entityId)
        {
            var request = new RestRequest("/{entityType}/{entityId}", Method.GET);
            request.AddUrlSegment("entityId", entityId);
            request.AddUrlSegment("entityType", entityType);
            var result = await Client.ExecuteAsync<List<Consent>>(request);
            return new List<IConsent>(result);
        }


    }
}