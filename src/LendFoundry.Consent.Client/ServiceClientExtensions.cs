﻿using LendFoundry.Security.Tokens;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Consent.Client
{
    public static class ConsentServiceClientExtensions
    {
        public static IServiceCollection AddConsentService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IConsentServiceClientFactory>(p => new ConsentServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IConsentServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
