﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using System.Collections.Generic;
#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif
using LendFoundry.Consent.Persistence;
using LendFoundry.DocumentGenerator.Client;
using LendFoundry.DocumentManager.Client;
using LendFoundry.TemplateManager.Client;
using LendFoundry.Application.Document.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Services;
using LendFoundry.Configuration;

namespace LendFoundry.Consent.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
                        // Register the Swagger generator, defining one or more Swagger documents
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "LendFoundryConsent"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme() 
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> 
                { 
					{ "Bearer", new string[]{} }
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.Consent.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif
            // services
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddConfigurationService<ConsentConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddDocumentGenerator(Settings.DocumentGenerator.Host, Settings.DocumentGenerator.Port);
            services.AddApplicantDocumentService(Settings.ApplicationDocument.Host, Settings.ApplicationDocument.Port);
            // document-manager
            services.AddDocumentManager(Settings.DocumentGenerator.Host, Settings.DocumentGenerator.Port);
            // template-manager
            services.AddTemplateManagerService(Settings.TemplateManager.Host, Settings.TemplateManager.Port);
            // internals
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient(p => p.GetService<IConfigurationService<ConsentConfiguration>>().Get());
            services.AddTransient<IConsentRepository, ConsentRepository>();
            services.AddTransient<IConsentService, ConsentService>();
            //services.AddTransient<IConsentConfiguration, ConsentConfiguration>();
            // interface resolver
            services.AddMvc();
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            #if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "LendFoundry Consent Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            //app.UseMiddleware<RequestLoggingMiddleware>();
            app.UseMvc();
            app.UseHealthCheck();
  
        }
        //public class RequestLoggingMiddleware
        //{
        //    private RequestDelegate Next { get; }

        //    public RequestLoggingMiddleware(RequestDelegate next)
        //    {
        //        Next = next;
        //    }

        //    public async Task Invoke(HttpContext context)
        //    {
        //        // The MVC middleware reads the body stream before we do, which
        //        // means that the body stream's cursor would be positioned at the end.
        //        // We need to reset the body stream cursor position to zero in order
        //        // to read it. As the default body stream implementation does not
        //        // support seeking, we need to explicitly enable rewinding of the stream:
        //        try
        //        {
        //            await Next.Invoke(context);
        //        }
        //        catch (Exception exception)
        //        {

        //        }
        //    }
        //}
    }
}
