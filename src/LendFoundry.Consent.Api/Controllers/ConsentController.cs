﻿using LendFoundry.Foundation.Services;
using LendFoundry.TemplateManager;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System.Threading.Tasks;

namespace LendFoundry.Consent.Api.Controllers
{
    [Route("/")]
    public class ConsentController : ExtendedController
    {
        public ConsentController(IConsentService consentService)
        {
            ConsentService = consentService;
        }

        private IConsentService ConsentService { get; }

        [HttpPost("{entityType}/{entityId}/{consentName}/sign")]
        [ProducesResponseType(typeof(IConsent[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Sign(string entityType, string entityId, string consentName,[FromBody]object body)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await ConsentService.Sign(entityType, entityId, consentName, body));
                }
                catch (ConcentException ex)
                {
                    return ErrorResult.BadRequest(ex.Message);
                }
                catch (ConsentAlreadySigned ex)
                {
                    return new ErrorResult(409, ex.Message);
                }
            });
        }

        [HttpGet("{entityType}/{entityId}")]
        [ProducesResponseType(typeof(IConsent[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetSignedConsentDocument(string entityType, string entityId)
        {
            return await ExecuteAsync(async () => Ok(await ConsentService.GetSignedConsentDocument(entityType, entityId)));
        }

        [HttpPost("{entityType}/{entityId}/{consentName}/view")]
        [ProducesResponseType(typeof(ITemplateResult[]), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> View(string entityType, string entityId, string consentName, [FromBody]object body)
        {
            return await ExecuteAsync(async () =>
            {
                var templateResult = await ConsentService.View(entityType, entityId, consentName, body);
                return Ok(templateResult);
            });
        }
    }
}
