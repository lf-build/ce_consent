using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.Consent.Api
{
    public static class Settings
    {
        public static string ServiceName { get; } = "application-consent";

        private static string Prefix { get; } = ServiceName.ToUpper();

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION_HOST", "configuration", $"{Prefix}_CONFIGURATION_PORT");

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");
        public static ServiceSettings DocumentGenerator { get; } = new ServiceSettings($"{Prefix}_DOCUMENTGENERATOR", "document-generator");

        public static ServiceSettings TemplateManager { get; } = new ServiceSettings($"{Prefix}_TEMPLATEMANAGER", "template-manager");

        public static ServiceSettings ApplicationDocument { get; } = new ServiceSettings($"{Prefix}_APPLICATIONDOCUMENT", "application-document");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "Consent");

        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats";
    }
}