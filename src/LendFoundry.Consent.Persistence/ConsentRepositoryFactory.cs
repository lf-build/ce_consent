﻿using LendFoundry.Consent;
using System;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif


namespace LendFoundry.Consent.Persistence
{
    public class ConsentRepositoryFactory
    {
        private IServiceProvider Provider { get; }

        public ConsentRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IConsentRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new ConsentRepository(tenantService, mongoConfiguration);
        }

    }
}
