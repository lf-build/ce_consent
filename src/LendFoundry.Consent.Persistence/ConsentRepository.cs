﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Consent.Persistence
{
    public class ConsentRepository : MongoRepository<IConsent, Consent>, IConsentRepository
    {
        #region Constructors
        static ConsentRepository()
        {
            BsonClassMap.RegisterClassMap<Consent>(map =>
            {
                map.AutoMap();
                var type = typeof(Consent);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }
        public ConsentRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "application-consent")
        {
            CreateIndexIfNotExists("Consents_applicationid", Builders<IConsent>.IndexKeys.Ascending(i => i.EntityId));
        }
        #endregion
        public async Task<IList<IConsent>> GetSignedConsentDocument(string entityType, string entityId)
        {
            return
                await Query.Where(consent => consent.EntityId == entityId && consent.EntityType == entityType).ToListAsync();
            //var filter = Builders<IConsent>.Filter.Where(consent => consent.EntityId == entityId && consent.EntityType == entityType);
            //return await Collection.Find(filter).ToListAsync();

        }

        public async Task<IConsent> GetConsentDocumentByConsentName(string entityType, string entityId, string consentName)
        {
            return
                await
                    Query.FirstOrDefaultAsync(
                        consent =>
                            consent.EntityType.Equals(entityType) &&
                            consent.EntityId.Equals(entityId) && consent.ConsentName.Equals(consentName));

            //var filter = Builders<IConsent>.Filter.Where(consent => consent.TenantId == TenantService.Current.Id && consent.EntityType.Equals(entityType) && consent.EntityId.Equals(entityId) && consent.ConsentName.Equals(consentName));
            //return await Collection.Find(filter).FirstOrDefaultAsync();
        }
    }
}
