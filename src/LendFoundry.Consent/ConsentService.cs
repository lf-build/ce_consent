﻿using LendFoundry.Application.Document;
using LendFoundry.Consent.Configuration;
using LendFoundry.DocumentGenerator;
#if DOTNET2
using LendFoundry.EventHub;
#else
using LendFoundry.EventHub.Client;
#endif
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.TemplateManager;
#if DOTNET2
using Microsoft.AspNetCore.Http;
#else
using Microsoft.AspNet.Http;
#endif
using Microsoft.CSharp.RuntimeBinder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Consent
{
    public class ConsentService : IConsentService
    {
        #region Constructors
        public ConsentService
        (
            IConsentRepository consentRepository,
            IEventHubClient eventHubClient,
            ITenantTime tenantTime,
            IDocumentGeneratorService documentGeneratorService,
            ITemplateManagerService templateManagerService,
            ConsentConfiguration consentConfigurations,
            IApplicationDocumentService applicationDocumentService,
            IHttpContextAccessor httpAccessor,
            ILogger logger
        )
        {
            TenantTime = tenantTime;
            ConsentRepository = consentRepository;
            EventHubClient = eventHubClient;
            DocumentGenerator = documentGeneratorService;
            TemplateManager = templateManagerService;
            ConsentConfigurations = consentConfigurations;
            ApplicationDocumentService = applicationDocumentService;
            HttpAccessor = httpAccessor;
            Logger = logger;
        }
        #endregion

        #region Private Properties
        private ITenantTime TenantTime { get; }
        private IConsentRepository ConsentRepository { get; }
        private IEventHubClient EventHubClient { get; }
        private IDocumentGeneratorService DocumentGenerator { get; }
        private ITemplateManagerService TemplateManager { get; }
        private ConsentConfiguration ConsentConfigurations { get; }

        private IApplicationDocumentService ApplicationDocumentService { get; }
        private IHttpContextAccessor HttpAccessor { get; }

        private ILogger Logger { get; }
        #endregion

        #region Public Methods       

        public async Task<IConsent> Sign(string entityType, string entityId, string concentName, object data)
        {
            EnsureInputIsValid(entityType, entityId, concentName, data);
            var configConsent = GetConsentFromConfiguration(entityType, concentName);
            if (configConsent == null)
                throw new NotFoundException($"{concentName} is not found in configuration for {entityType}.");

            var isExistsConsent = await ConsentRepository.GetConsentDocumentByConsentName(entityType, entityId, concentName);
            if (isExistsConsent != null)
                throw new ConsentAlreadySigned($"{concentName} is already signed for {entityId} .");

            var document = new Document
            {
                Data = data,
                Format = DocumentFormat.Pdf,
                Name = configConsent.Document.Name,
                Version = configConsent.TemplateVersion
            };

            var documentResult = await DocumentGenerator.Generate(configConsent.TemplateName, configConsent.TemplateVersion, Format.Html, document);

            if (documentResult?.Content == null || documentResult.Content.Length == 0)
                throw new NotFoundException($"The Document with template name:{nameof(configConsent.TemplateName)} is not found");

            #region Meta Data 

            var objMetaData = new MetaData();
            objMetaData.TemplateName = configConsent.TemplateName;
            objMetaData.TemplateVersion = configConsent.TemplateVersion; 

            #endregion

            var uploadedDocument = await ApplicationDocumentService.Add(entityType, entityId, configConsent.Document.Category, documentResult.Content, configConsent.Document.Name, configConsent.Document.Tags, objMetaData);

            if (uploadedDocument == null)
                throw new ConcentException("The Document cannot be uploaded");

            if (string.IsNullOrWhiteSpace(uploadedDocument.Id))
                throw new ConcentException($"{nameof(uploadedDocument)} is not found");
            var consent = new Consent
            {
                ConsentName = concentName,
                DocumentId = uploadedDocument.DocumentId,
                EntityId = entityId,
                EntityType = entityType,
                IpAddress = GetIpAddressValue(data),
                SignedBy = GetSignedByValue(data),
                SignedDate = new TimeBucket(TenantTime.Now)
            };

            ConsentRepository.Add(consent);
            await EventHubClient.Publish(new Events.ConsentCreated(consent));
            return consent;
        }
        private void EnsureInputIsValid(string entityType, string entityId, string consentName, object data)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException($"{nameof(entityType)} cannot be empty");

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException($"{nameof(entityId)} cannot be empty");

            if (string.IsNullOrWhiteSpace(consentName))
                throw new ArgumentNullException($"{nameof(consentName)} cannot be empty");

            //if (string.IsNullOrWhiteSpace(GetIpAddressValue(data)))
            //    throw new ArgumentException("IPAddress can not be empty");

            if (string.IsNullOrWhiteSpace(GetSignedByValue(data)))
                throw new ArgumentException("Signed By can not be empty");
        }

        public async Task<IList<IConsent>> GetSignedConsentDocument(string entityType, string entityId)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException($"{nameof(entityType)} cannot be empty");

            if (string.IsNullOrWhiteSpace(entityId))
                throw new ArgumentNullException($"{nameof(entityId)} cannot be empty");

            // stop process in case of not found bank to update
            var documents = await ConsentRepository.GetSignedConsentDocument(entityType, entityId);
            if (documents == null || !documents.Any())
            {
                throw new NotFoundException($"No signed consent is found for {nameof(entityId)}");
            }
            return documents;
        }

        public async Task<ITemplateResult> View(string entityType, string entityId, string consentName, object data)
        {
            if (string.IsNullOrWhiteSpace(entityType))
                throw new ArgumentNullException($"{nameof(entityType)} cannot be empty");

            if (string.IsNullOrWhiteSpace(consentName))
                throw new ArgumentNullException($"{nameof(consentName)} cannot be empty");

            var consent = GetConsentFromConfiguration(entityType, consentName);

            if (consent == null)
                throw new NotFoundException($"The consent:{consentName} was not found");

            var template = await TemplateManager.Get(consent.TemplateName, consent.TemplateVersion, Format.Html);

            if (template == null)
                throw new NotFoundException($"The TemplateName:{consent.TemplateName} was not found");

            var templateResult = await TemplateManager.Process(template.Name, template.Version, template.Format, data);

            if (templateResult == null)
                throw new NotFoundException($"The TemplateName:{consent.TemplateName} was not found or cannot be processed");

            return new TemplateResult { Title = templateResult.Title, Data = templateResult.Data };
        }

        #endregion

        #region Private Methods
        private ConsentConfigurationConsent GetConsentFromConfiguration(string entityType, string consentName)
        {
            if (ConsentConfigurations == null)
                throw new NotFoundException($"The entity:{entityType} was not found in configuration");

            var consentConfigurationEntity = ConsentConfigurations.Entities.FirstOrDefault();
            if (consentConfigurationEntity != null && (ConsentConfigurations.Entities == null || !ConsentConfigurations.Entities.Any() || string.IsNullOrEmpty(consentConfigurationEntity.EntityName)))
                throw new NotFoundException($"The entity:{entityType} was not found in configuration");

            var entity = ConsentConfigurations.Entities.FirstOrDefault(e => e.EntityName.Equals(entityType, StringComparison.OrdinalIgnoreCase));

            if (entity == null)
                throw new NotFoundException($"The entity:{entityType} was not found in configuration");

            if (entity.Consents == null)
                throw new NotFoundException($"The entity:{entityType} does not have consent schemas");

            return entity.Consents.FirstOrDefault(c => c.Name.Equals(consentName, StringComparison.OrdinalIgnoreCase));
        }

        private string GetIpAddressValue(dynamic data)
        {
            Func<dynamic> ipAddressProperty = () => data.IPAddress;

            var ipAddress = HasProperty(ipAddressProperty) ? GetValue(ipAddressProperty) : string.Empty;
            if (ipAddress != null && !string.IsNullOrWhiteSpace(ipAddress.ToString()) && !string.IsNullOrWhiteSpace(ipAddress.Value))
                return ipAddress.Value;
            return GetClientIP();
        }

        private string GetClientIP()
        {
            const string message = "Cannot determine client IP";
            try
            {
                if (HttpAccessor == null)
                    throw new ArgumentException(message);
                if (HttpAccessor.HttpContext == null)
                    throw new ArgumentNullException(message);
                if (HttpAccessor.HttpContext.Request == null)
                    throw new ArgumentNullException(message);
                if (!HttpAccessor.HttpContext.Request.Headers.Any(h => h.Key.ToLower() == "x-client-ip"))
                    throw new ArgumentNullException(message);
                return HttpAccessor.HttpContext.Request.Headers["X-Client-IP"];
            }
            catch (Exception ex)
            {
                Logger.Error("Missing IP address in consent");
                return null;
                //throw new ArgumentException($"The method GetClientIP('{message}') raised an error\n: {ex.Message}");
            }
        }

        private static string GetSignedByValue(dynamic data)
        {
            Func<dynamic> signedByProperty = () => data.SignedBy;

            return HasProperty(signedByProperty) ? GetValue(signedByProperty) : string.Empty;
        }

        private static bool HasProperty<T>(Func<T> property)
        {
            try
            {
                property();
                return true;
            }
            catch (RuntimeBinderException)
            {
                return false;
            }
        }

        private static T GetValue<T>(Func<T> property)
        {
            return property();
        }

        #endregion

    }
}