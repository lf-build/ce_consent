﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Consent
{
    public class Consent : Aggregate, IConsent
    {
        public string EntityId { get; set; }
        public string EntityType { get; set; }
        public string ConsentName { get; set; }
        public string DocumentId { get; set; }
        public TimeBucket SignedDate { get; set; }
        public string IpAddress { get; set; }
        public string SignedBy { get; set;}
    }
}
