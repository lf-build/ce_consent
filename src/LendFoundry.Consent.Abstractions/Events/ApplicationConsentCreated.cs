﻿
namespace LendFoundry.Consent.Events
{
    public class ConsentCreated
    {
        public ConsentCreated(IConsent consent)
        {
            Consent = consent;
        }
        public IConsent Consent { get; set; }
    }
}
