﻿namespace LendFoundry.Consent.Configuration
{
    public class ConsentConfigurationEntity
    {
        public string EntityName { get; set; }

        public ConsentConfigurationConsent[] Consents { get; set; }
    }
}
