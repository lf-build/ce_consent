﻿using System.Collections.Generic;

namespace LendFoundry.Consent.Configuration
{
    public class ConsentConfigurationConsent
    {
        public string Name { get; set; }
        public string TemplateName { get; set; }
        public string TemplateVersion { get; set; }
        public ConsentTrackerDocument Document { get; set; }
    }
}
