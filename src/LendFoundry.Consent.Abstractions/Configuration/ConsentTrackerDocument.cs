﻿using System.Collections.Generic;

namespace LendFoundry.Consent.Configuration
{
    public class ConsentTrackerDocument
    {
        public string Name { get; set; }
        public List<string> Tags { get; set; }
        public string Category { get; set; }
    }
}