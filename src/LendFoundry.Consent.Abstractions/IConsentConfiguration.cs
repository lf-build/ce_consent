﻿using LendFoundry.Consent.Configuration;

namespace LendFoundry.Consent
{
    public interface IConsentConfiguration
    {
        ConsentConfigurationConsent[] Concents { get; set; }
    }
}