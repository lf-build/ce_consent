﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Consent
{
    [Serializable]
    public class ConcentException : Exception
    {
        public ConcentException()
        {
        }

        public ConcentException(string message) : base(message)
        {
        }

        public ConcentException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ConcentException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}