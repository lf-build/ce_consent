﻿using LendFoundry.DocumentGenerator;
using LendFoundry.TemplateManager;
using System.Collections.Generic;


namespace LendFoundry.Consent
{
    public interface IConsentRequest : IConsent , ITemplate
    {
        string FileName { get; set; }
        DocumentFormat DocumentFormat { get; set; }
        List<string> Tags { get; set; }
        string DocumentVersion { get; set; }
    }
}
