﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Consent
{
    public interface IConsent : IAggregate
    {
        string EntityId { get; set; }
        string EntityType { get; set; }
        //TOdo Lookup Services
        string ConsentName { get; set; }
        string DocumentId { get; set; }
        TimeBucket SignedDate { get; set; }
        string IpAddress { get; set; }
        string SignedBy { get; set;}
    }
}
