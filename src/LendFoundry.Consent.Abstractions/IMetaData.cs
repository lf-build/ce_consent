﻿namespace LendFoundry.Consent
{
    public interface IMetaData
    {
        string TemplateName { get; set; }
        string TemplateVersion { get; set; }
    }
}
