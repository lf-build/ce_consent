﻿using LendFoundry.Consent.Configuration;

namespace LendFoundry.Consent
{
    public class ConsentConfiguration 
    {
        public ConsentConfigurationEntity[] Entities { get; set; }
    }
  
}
