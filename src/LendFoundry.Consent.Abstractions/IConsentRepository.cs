﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using System.Threading.Tasks;

namespace LendFoundry.Consent
{
    public interface IConsentRepository : IRepository<IConsent>
    {
        Task<IList<IConsent>> GetSignedConsentDocument(string entityType, string entityId);
        Task<IConsent> GetConsentDocumentByConsentName(string entityType, string entityId, string consentName);

    }
}