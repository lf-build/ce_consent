﻿using System;
using System.Runtime.Serialization;

namespace LendFoundry.Consent
{
    [Serializable]
    public class ConsentAlreadySigned : Exception
    {
        public ConsentAlreadySigned()
        {
        }

        public ConsentAlreadySigned(string message) : base(message)
        {
        }

        public ConsentAlreadySigned(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ConsentAlreadySigned(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}