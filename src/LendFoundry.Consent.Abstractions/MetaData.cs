﻿namespace LendFoundry.Consent
{
    public class MetaData : IMetaData
    {
        public string TemplateName { get; set; }
        public string TemplateVersion { get; set; }
    }
}
