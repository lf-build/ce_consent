﻿using LendFoundry.TemplateManager;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Consent
{
    public interface IConsentService
    {
        Task<IConsent> Sign(string entityType, string entityId, string concentName, object body);
        Task<ITemplateResult> View(string entityType, string entityId, string consentName, object data);
        Task<IList<IConsent>> GetSignedConsentDocument(string entityType, string entityId);
    }
}
